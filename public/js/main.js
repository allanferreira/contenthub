$(function(){
    function init(){
        setTimeout(function(){
            var section1 = $('section.banner').outerHeight();
            var section2 = $('section.quemsomos').outerHeight();
            var section3 = $('section.beneficios').outerHeight();
            var section4 = $('section.multidevice').outerHeight();
            var section5 = $('section.auto').outerHeight();
            var section6 = $('section.orientacao').outerHeight();
            var section7 = $('section.dashboard').outerHeight();
            var section8 = $('section.infra').outerHeight();
            var section9 = $('section.cases').outerHeight();
            var section10 = $('section.planos').outerHeight();
            var section11 = $('section.contact').outerHeight();

            $(window).scroll(function () {
                if($(this).scrollTop() >= section1){
                    $('nav.menu li').removeClass('active');
                    $('nav.menu li[data-value="quemsomos"]').addClass('active');
                } else{
                    $('nav.menu li').removeClass('active');
                }
                if($(this).scrollTop() >= section1+section2){
                    $('nav.menu li').removeClass('active');
                    $('nav.menu li[data-value="beneficios"]').addClass('active');
                }
                if($(this).scrollTop() >= section1+section2+section3+section4+section5+section6+section7+section8){
                    $('nav.menu li').removeClass('active');
                    $('nav.menu li[data-value="cases"]').addClass('active');
                }
                if($(this).scrollTop() >= section1+section2+section3+section4+section5+section6+section7+section8+section9){
                    $('nav.menu li').removeClass('active');
                    $('nav.menu li[data-value="planos"]').addClass('active');
                }
                if($(this).scrollTop() >= section1+section2+section3+section4+section5+section6+section7+section8+section9+section10){
                    $('nav.menu li').removeClass('active');
                    $('nav.menu li[data-value="contato"]').addClass('active');
                }
            });
            
            $('*[data-value]').click(function(){
                switch($(this).data('value')) {
                    case "banner":
                        $('hmtl,body').animate({
                            scrollTop: 0
                        });
                        break;
                    case "quemsomos":
                        $('hmtl,body').animate({
                            scrollTop: section1
                        });
                        break;
                    case "beneficios":
                        $('hmtl,body').animate({
                            scrollTop: section1+section2
                        });
                        break;
                    case "multidevices":
                        $('hmtl,body').animate({
                            scrollTop: section1+section2+section3
                        });
                        break;
                    case "automacao":
                        $('hmtl,body').animate({
                            scrollTop: section1+section2+section3+section4
                        });
                        break;
                    case "business":
                        $('hmtl,body').animate({
                            scrollTop: section1+section2+section3+section4+section5
                        });
                        break;
                    case "dashboard":
                        $('hmtl,body').animate({
                            scrollTop: section1+section2+section3+section4+section5+section6
                        });
                        break;
                    case "infraestrutura":
                        $('hmtl,body').animate({
                            scrollTop: section1+section2+section3+section4+section5+section6+section7
                        });
                        break;
                    case "cases":
                        $('hmtl,body').animate({
                            scrollTop: section1+section2+section3+section4+section5+section6+section7+section8
                        });
                        break;
                    case "planos":
                        $('hmtl,body').animate({
                            scrollTop: section1+section2+section3+section4+section5+section6+section7+section8+section9
                        });
                        break;
                    case "contato":
                        $('hmtl,body').animate({
                            scrollTop: section1+section2+section3+section4+section5+section6+section7+section8+section9+section10
                        });
                        break;
                }
            });

            $('.blank').addClass('animated');
        },500);
    }
    init();
    $(window).resize(function() {
        init();
    });



    setTimeout(function(){
        $('.blank').remove();
        $('.banner_buttons').removeClass('animation--fix').addClass('bounceInUp animated');
    },1000);
    
    var h = $(window).height();
    $(window).scroll(function () {
        if ($(window).width() > 767) {
            if ($(this).scrollTop() >= h) {
               $('.header').addClass('header--fixed');
               $('.banner').addClass('banner--fix-scroll');
            } else {
               $('.header').removeClass('header--fixed');
               $('.banner').removeClass('banner--fix-scroll');
            }
        }
    });


    //////////
    // LIBS //
    //////////
    
    //init carousel
    
    $('.owl-carousel-banner,.owl-carousel-cases').owlCarousel({
        loop:false,
        margin:0,
        items:1,
        autoplay:false,
        nav: false,
        dots: false
    });
    $('.owl-carousel-auto,.owl-carousel-dashboard').owlCarousel({
        loop:false,
        margin:0,
        items:1,
        animateOut: 'fadeOut',
        autoplay:false
    });
    $('.auto .window__square').click(function(){
        $('.auto .window__square').removeClass('active');
        $(this).addClass('active');
        var data = $(this).data('target');
        $('.owl-carousel-auto').trigger('to.owl.carousel', data);
    });

    $('.dashboard .window__square').click(function(){
        $('.dashboard .window__square').removeClass('active');
        $(this).addClass('active');
        var data = $(this).data('target');
        $('.owl-carousel-dashboard').trigger('to.owl.carousel', data);
    });
    $('.cases .cases__nav .circle').click(function(){
        $('.cases .cases__nav .circle').removeClass('active');
        $(this).addClass('active');
        var data = $(this).data('target');
        $('.owl-carousel-cases').trigger('to.owl.carousel', data);
    });

    $('.button--color').click(function(){
        $('.table--planos .box').show('fold');

    });

    $('.box__close').click(function(){
        $('.table--planos .box').hide('fold');
    });
});
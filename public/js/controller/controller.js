app
	.controller('MainCtrl', ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {

	    //menu__open transition on click
	    $scope.toggleMenu = function(){
			$('.wrap').toggleClass('menu--open');
	    }

	    $scope.planChange = function(planname){
	    	$scope.plano.plan = planname;
	    }

	    //contact send
		$scope.contact = [];
		$scope.plano = [];
		$scope.sendContact = function(contact){
			
			$http({
	          url: './sendMail',
	          method: 'POST',
	          data: {
	            'form': 'Formulário de Contato',
	            'name': contact.name,
	            'email': contact.email,
	            'phone': contact.phone,
	            'company': contact.company,
	            'domain': contact.domain,
	            'message': contact.message
	          },
	          headers: {
	            'Accept': 'application/json',
	            'Content-Type': 'application/json'
	          }
	        }).
	        success(function (data) {
	            $scope.f1success = true;
	            $scope.contact = [];
	            $scope.formContact.$setPristine();

	            $timeout(function(){ 
	            	$('.contact .alert-success').hide('fade');
	            }, 4000);
	            $timeout(function(){ 
	            	$scope.f1success = false;
	            }, 5000);
	        }).
	        error(function (data) {
	            $scope.f1error = true;    
	        }); 
		}
		$scope.sendPlano = function(plano){
			$http({
	          url: './sendMail',
	          method: 'POST',
	          data: {
	            'form': 'Formulário de Planos',
	            'plan': plano.plan,
	            'name': plano.name,
	            'email': plano.email,
	            'phone': plano.phone,
	            'company': plano.company,
	            'domain': plano.domain,
	            'message': plano.message
	          },
	          headers: {
	            'Accept': 'application/json',
	            'Content-Type': 'application/json'
	          }
	        }).
	        success(function (data) {
	            $scope.f2success = true;
	            $scope.plano = [];
	            $scope.formPlanos.$setPristine();

	            $timeout(function(){ 
	            	$('.planos .alert-success').hide('fade');
	            }, 4000);
	            $timeout(function(){ 
	            	$scope.f2success = false;
	            }, 5000);
	        }).
	        error(function (data) {
	            $scope.f2error = true;    
	        });
		}
	}])